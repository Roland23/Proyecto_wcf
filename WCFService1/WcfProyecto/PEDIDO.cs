//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfProyecto
{
    using System;
    using System.Collections.Generic;
    
    public partial class PEDIDO
    {
        public PEDIDO()
        {
            this.DETALLE_PEDIDO = new HashSet<DETALLE_PEDIDO>();
        }
    
        public int Id { get; set; }
        public string Fecha { get; set; }
        public int USUARIOId { get; set; }
        public int MESAId { get; set; }
        public string Estado { get; set; }
    
        public virtual USUARIO USUARIO { get; set; }
        public virtual MESA MESA { get; set; }
        public virtual ICollection<DETALLE_PEDIDO> DETALLE_PEDIDO { get; set; }
    }
}
