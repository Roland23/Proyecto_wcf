﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfProyecto
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarLogin/{Id}", ResponseFormat = WebMessageFormat.Json)]
        string ConsultarLogin(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarMesas", ResponseFormat = WebMessageFormat.Json)]
        List<MESA> MostrarMesas();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarMesa/{Id}", ResponseFormat = WebMessageFormat.Json)]
        MESA ConsultarMesa(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPlatos", ResponseFormat = WebMessageFormat.Json)]
        List<PLATO> MostrarPlatos();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPlatos", ResponseFormat = WebMessageFormat.Json)]
        PLATO ConsultarPlato(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostrarPedidos", ResponseFormat = WebMessageFormat.Json)]
        List<PEDIDO> MostrarPedidos();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarPedido/{Id}", ResponseFormat = WebMessageFormat.Json)]
        PEDIDO ConsultarPedido(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearPedido", ResponseFormat = WebMessageFormat.Json)]
        void CrearPedido(PEDIDO x);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ModificarPedido", ResponseFormat = WebMessageFormat.Json)]
        void ModidicarPedido(PEDIDO x);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ModificarEstadoPedido", ResponseFormat = WebMessageFormat.Json)]
        void ModificarEstadoPedido(PEDIDO x);
    }
}
