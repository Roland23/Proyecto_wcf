
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/28/2015 14:09:27
-- Generated from EDMX file: C:\Users\Roland\Documents\Visual Studio 2013\Projects\WCFService1\WcfProyecto\ERrestaurante.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Bdrestaurante];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PEDIDOUSUARIO]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PEDIDOSet] DROP CONSTRAINT [FK_PEDIDOUSUARIO];
GO
IF OBJECT_ID(N'[dbo].[FK_PEDIDOMESA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PEDIDOSet] DROP CONSTRAINT [FK_PEDIDOMESA];
GO
IF OBJECT_ID(N'[dbo].[FK_DETALLE_PEDIDOPEDIDO]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DETALLE_PEDIDOSet] DROP CONSTRAINT [FK_DETALLE_PEDIDOPEDIDO];
GO
IF OBJECT_ID(N'[dbo].[FK_DETALLE_PEDIDOPLATO]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DETALLE_PEDIDOSet] DROP CONSTRAINT [FK_DETALLE_PEDIDOPLATO];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[INFORMACIONSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[INFORMACIONSet];
GO
IF OBJECT_ID(N'[dbo].[MESASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MESASet];
GO
IF OBJECT_ID(N'[dbo].[PEDIDOSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PEDIDOSet];
GO
IF OBJECT_ID(N'[dbo].[DETALLE_PEDIDOSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DETALLE_PEDIDOSet];
GO
IF OBJECT_ID(N'[dbo].[PLATOSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PLATOSet];
GO
IF OBJECT_ID(N'[dbo].[USUARIOSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USUARIOSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'INFORMACIONSet'
CREATE TABLE [dbo].[INFORMACIONSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Razon_Social] nvarchar(max)  NOT NULL,
    [NIT] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MESASet'
CREATE TABLE [dbo].[MESASet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PEDIDOSet'
CREATE TABLE [dbo].[PEDIDOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Fecha] nvarchar(max)  NOT NULL,
    [USUARIOId] int  NOT NULL,
    [MESAId] int  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DETALLE_PEDIDOSet'
CREATE TABLE [dbo].[DETALLE_PEDIDOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cantidad] nvarchar(max)  NOT NULL,
    [PEDIDOId] int  NOT NULL,
    [PLATOId] int  NOT NULL
);
GO

-- Creating table 'PLATOSet'
CREATE TABLE [dbo].[PLATOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tipo] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'USUARIOSet'
CREATE TABLE [dbo].[USUARIOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Documento] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Cargo] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'INFORMACIONSet'
ALTER TABLE [dbo].[INFORMACIONSet]
ADD CONSTRAINT [PK_INFORMACIONSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MESASet'
ALTER TABLE [dbo].[MESASet]
ADD CONSTRAINT [PK_MESASet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PEDIDOSet'
ALTER TABLE [dbo].[PEDIDOSet]
ADD CONSTRAINT [PK_PEDIDOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DETALLE_PEDIDOSet'
ALTER TABLE [dbo].[DETALLE_PEDIDOSet]
ADD CONSTRAINT [PK_DETALLE_PEDIDOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PLATOSet'
ALTER TABLE [dbo].[PLATOSet]
ADD CONSTRAINT [PK_PLATOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USUARIOSet'
ALTER TABLE [dbo].[USUARIOSet]
ADD CONSTRAINT [PK_USUARIOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [USUARIOId] in table 'PEDIDOSet'
ALTER TABLE [dbo].[PEDIDOSet]
ADD CONSTRAINT [FK_PEDIDOUSUARIO]
    FOREIGN KEY ([USUARIOId])
    REFERENCES [dbo].[USUARIOSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PEDIDOUSUARIO'
CREATE INDEX [IX_FK_PEDIDOUSUARIO]
ON [dbo].[PEDIDOSet]
    ([USUARIOId]);
GO

-- Creating foreign key on [MESAId] in table 'PEDIDOSet'
ALTER TABLE [dbo].[PEDIDOSet]
ADD CONSTRAINT [FK_PEDIDOMESA]
    FOREIGN KEY ([MESAId])
    REFERENCES [dbo].[MESASet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PEDIDOMESA'
CREATE INDEX [IX_FK_PEDIDOMESA]
ON [dbo].[PEDIDOSet]
    ([MESAId]);
GO

-- Creating foreign key on [PEDIDOId] in table 'DETALLE_PEDIDOSet'
ALTER TABLE [dbo].[DETALLE_PEDIDOSet]
ADD CONSTRAINT [FK_DETALLE_PEDIDOPEDIDO]
    FOREIGN KEY ([PEDIDOId])
    REFERENCES [dbo].[PEDIDOSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DETALLE_PEDIDOPEDIDO'
CREATE INDEX [IX_FK_DETALLE_PEDIDOPEDIDO]
ON [dbo].[DETALLE_PEDIDOSet]
    ([PEDIDOId]);
GO

-- Creating foreign key on [PLATOId] in table 'DETALLE_PEDIDOSet'
ALTER TABLE [dbo].[DETALLE_PEDIDOSet]
ADD CONSTRAINT [FK_DETALLE_PEDIDOPLATO]
    FOREIGN KEY ([PLATOId])
    REFERENCES [dbo].[PLATOSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DETALLE_PEDIDOPLATO'
CREATE INDEX [IX_FK_DETALLE_PEDIDOPLATO]
ON [dbo].[DETALLE_PEDIDOSet]
    ([PLATOId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------